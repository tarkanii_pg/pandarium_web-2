package com.playtagon.pandarium_web;

import java.util.List;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.util.Clients;

import com.mongodb.MongoClient;

public class IndexViewModel {
	private String emailVal;
	private EmailValidator validator = new EmailValidator();

	@Init
	public void init() {
		emailVal = "Leave your mail and we will keep you informed";
	}

	public String getEmailVal() {
		return emailVal;
	}

	public void setEmailVal(String emailVal) {
		this.emailVal = emailVal;
	}

	public EmailValidator getValidator() {
		return validator;
	}

	@Command
	public void saveEmail() {
		System.out.println("Save email!!!");

		MongoClient mongoClient = new MongoClient("localhost");
		Morphia morphia = new Morphia();
		Datastore datastore = morphia.createDatastore(mongoClient, "pandarium");

		Query<Email> emailQuery = datastore.createQuery(Email.class);
		emailQuery.and(emailQuery.criteria("data").equal(emailVal));
		List<Email> emails = emailQuery.asList();
		if (emails.size() > 0) {
			Clients.showNotification("This email is registered");
		} else {
			Email email = new Email(emailVal);
			datastore.save(email);
		}
	}
}
